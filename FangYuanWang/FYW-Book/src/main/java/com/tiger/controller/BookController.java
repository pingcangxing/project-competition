package com.tiger.controller;


import com.tiger.exception.ServiceException;
import com.tiger.pojo.JsonResult;
import com.tiger.pojo.book.Book;
import com.tiger.pojo.user.User;
import com.tiger.service.BookService;
import com.tiger.utils.CookieUtil;
import com.tiger.utils.ObjectMapperUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/book/")
public class BookController {
	@Autowired
	private BookService bookService;
	@Autowired
	JedisCluster jedisCluster;
	/**
	 * 保存预约信息
	 * @param book
	 * @return
	 */
	@PostMapping("doSave")
	public JsonResult doSave(@RequestBody Book book) {
		//后面根据shiro框架模型解封
		//String userName=SecurityUtils.getSubject().getPrincipal().getUserName();


		return bookService.doSave(book); 
	}
	
	/*
	 * 跳转页面
	 */
	@RequestMapping("doFindBook")
	public JsonResult doFindBook(HttpServletRequest request) {
		//后面根据shiro框架模型解封
		//String userName=SecurityUtils.getSubject().getPrincipal().getUserName();
		String ticket = CookieUtil.getCookieValue(request, "FYW_TICKET");
		String json = jedisCluster.get(ticket);
		User user = ObjectMapperUtil.toObj(json, User.class);
		String userName=user.getUserName();
		System.out.println(userName);
		if(userName==null){
			throw new ServiceException("没有此用户");
		}
		List<Book> list=bookService.FindBook(userName);
		
		return new JsonResult(list); 
		}
	
	/**
	 * 删除操作
	 * @param ids
	 * @return 删除行数
	 */
	@RequestMapping("doDeleteObjects")
	public JsonResult doDeleteObjects(Integer[] ids) {
		
		
		int rows=bookService.doDeleteObjectsByIds(ids);
		
		return new JsonResult(rows); 
		}
	
	
	@RequestMapping("doUpdateObject")
	public JsonResult doUpdateObject(Book book) {
		
		
		int rows=bookService.doUpdateObject(book);
		
		return new JsonResult(rows); 
		}
	
	
}

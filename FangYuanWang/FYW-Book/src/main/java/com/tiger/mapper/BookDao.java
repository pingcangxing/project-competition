package com.tiger.mapper;


import com.tiger.pojo.book.Book;
import org.apache.ibatis.annotations.*;
import org.checkerframework.checker.propkey.qual.PropertyKey;

import java.util.List;


@Mapper
public interface BookDao {
	@Insert("insert into booklist(userName,tel,bookTime,manager,managerTel,address,state) values(#{userName},#{tel},#{bookTime},#{manager},#{managerTel},#{address},#{state})")
	@Options(keyProperty = "id",useGeneratedKeys = true)
	int doSave(Book book);

	@Select("select id,userName,tel,bookTime,manager,managerTel,address,state from booklist where userName=#{userName} order by bookTime desc")
	List<Book> findBook(String userName);

	@Update("update booklist set state=0 where id=#{id}")
	void updateState(Long id);

	/*@Delete({"<script>delete from booklist where <foreach collection='id' separator='or' item='id'>id=#{id}</foreach></script>"})*/
	@Delete("<script>delete from booklist where id in <foreach collection='ids' open='(' item='id' separator=',' close=')'> #{id}</foreach></script>")
			int doDeleteObjectsByIds(@Param("ids") Integer[] ids);
	
	//修改预约信息
	@Update("update booklist set tel=#{tel},bookTime=#{bookTime} where id=#{id}")
	int doUpdateObject(Book book);

}

package com.tiger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class PageController {
	
	@RequestMapping("IndexUI")
	public String findIndexUI() {
		return "index";
	}
	
	@RequestMapping("{page}")
	public String findPage(@PathVariable String page) {
		return page;
	}
	
	
	@RequestMapping("doLoginUI")
	public String doLoginUI() {
		return "login";
	}
	@RequestMapping("doRegisterUI")
	public String doRegisterUI() {
		return "register";
	}
	
}

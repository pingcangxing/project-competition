package com.tiger.controller;


import com.tiger.pojo.JsonResult;
import com.tiger.pojo.book.Book;
import com.tiger.pojo.user.User;
import com.tiger.service.BookFeignService;
import com.tiger.service.BookService;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/book/")
public class BookController {
	@Autowired
	private BookFeignService bookFeignService;
	/**
	 * 保存预约信息
	 * @param book
	 * @return
	 */
	@RequestMapping("doSave")
	
	public JsonResult doSave(Book book) {
		//后面根据shiro框架模型解封
		User user= (User)SecurityUtils.getSubject().getPrincipal();
		String userName=user.getUserName();

				book.setUserName(userName);
				
		return bookFeignService.doSave(book);
	}

	
	
}

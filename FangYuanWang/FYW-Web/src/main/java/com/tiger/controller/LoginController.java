package com.tiger.controller;

import com.tiger.pojo.JsonResult;
import com.tiger.pojo.user.User;
import com.tiger.service.ShiroUserRealm;
import com.tiger.utils.CookieUtil;
import com.tiger.utils.ObjectMapperUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import redis.clients.jedis.JedisCluster;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;


@Controller
@RequestMapping("/user/")
@ResponseBody
public class LoginController {

	
	@Autowired
	private ShiroUserRealm shiroUserRealm;
	@Autowired
	private JedisCluster jedisCluster;

	@RequestMapping("doLogin")
	public JsonResult doLogin(HttpServletResponse response, String userName, String password) {
		System.out.println("==doLogin===");
		//获取Subject对象(负责提交客户端的账号信息)
		Subject subject=SecurityUtils.getSubject();
		UsernamePasswordToken token=new UsernamePasswordToken();
		token.setUsername(userName);
		token.setPassword(password.toCharArray());
		//提交账号信息给securityManager对象
		subject.login(token);
		User user= (User)SecurityUtils.getSubject().getPrincipal();
		String ticket= UUID.randomUUID().toString();

		//如果将数据保存到第三方，一般需要脱敏
		user.setPassword("123456");
		String json = ObjectMapperUtil.toJson(user);
		jedisCluster.setex(ticket, 7*24*60*60,json);
		
		CookieUtil.addCookie(response, "FYW_TICKET", ticket,7*24*60*60,"fyw.com");

		return new JsonResult("login ok");
	}
}

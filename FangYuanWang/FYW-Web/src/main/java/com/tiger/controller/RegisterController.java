package com.tiger.controller;

import java.util.HashMap;
import java.util.Map;

import com.tiger.pojo.JsonResult;
import com.tiger.pojo.user.User;
import com.tiger.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/register/")
public class RegisterController {
	Map<String,String> verificationCode=new HashMap<String,String>();
	@Autowired
	RegisterService registerService;
	/**
	 * 注册用户
	 * @param user
	 * @return
	 */
	@RequestMapping("saveRegister")
	public JsonResult saveRegister(User user, String code) {
		String tel = user.getTel();
		 String checkCode= verificationCode.get(tel);
		 //验证码校验
		 if(checkCode==null) {
			 throw new IllegalArgumentException("验证码错误");
		 }
			 if(checkCode.equals(code))
			 {
			 verificationCode.remove(tel);}
			 else {
				 throw new IllegalArgumentException("验证码错误");
			 }
		 
		 registerService.saveRegister(user);
		 return new JsonResult("注册成功");
	}
	
	/**
	 * 获取验证码
	 * @param
	 * @return
	 */
	@RequestMapping("getCode")
	public JsonResult getCode(String tel) {
		//先查找手机号是否注册过
		registerService.findByTel(tel);
		 String code = registerService.verificationCode(tel);
		 
		 verificationCode.put(tel, code);
		 return new JsonResult("验证码发送成功");
	}
}

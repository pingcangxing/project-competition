package com.tiger.service;

import com.tiger.pojo.user.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "sso-service")
public interface UserFingerService {
    @GetMapping("/findUser/username")
    User findUserByUserName(@RequestParam String username);

    @PostMapping("/saveRegister")
    Integer saveRegister(@RequestBody User user);

    @GetMapping("/findByTel/tel")
    String findByTel(@RequestParam String tel);
}

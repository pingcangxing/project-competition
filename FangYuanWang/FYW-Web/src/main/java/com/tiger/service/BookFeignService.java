package com.tiger.service;

import com.tiger.pojo.JsonResult;
import com.tiger.pojo.book.Book;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "book-service")
public interface BookFeignService {
    @PostMapping("/book/doSave")
    JsonResult doSave(@RequestBody Book book);
}

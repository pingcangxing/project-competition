package com.tiger.config;

import com.tiger.pojo.user.User;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;

import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@Configuration
public class SpringShiroConfig {

	@Bean
	public SecurityManager securityManager(Realm realm) {
		DefaultWebSecurityManager sManager= new DefaultWebSecurityManager();
		sManager.setRealm(realm);
		return sManager;
	}
	@Bean
	public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
		ShiroFilterFactoryBean sfBean= new ShiroFilterFactoryBean();
		sfBean.setSecurityManager(securityManager);
		sfBean.setLoginUrl("/doLoginUI");
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		map.put("/bootstrap/**", "anon");
		map.put("/css/**", "anon");
		map.put("/datepicker/**", "anon");
		
		map.put("/fonts/**", "anon");
		
		
		map.put("/img/**", "anon");
		map.put("/js/**", "anon");
		map.put("/layui/**", "anon");
		map.put("/IndexUI/**", "anon");
		map.put("/doRegisterUI/**", "anon");
		 map.put("/user/doLogin","anon");
		 map.put("/register/saveRegister","anon");
		 map.put("/register/getCode","anon");
		map.put("/findByTel/tel","anon");
		map.put("/findUser/username","anon");
		map.put("/saveRegister","anon");
		map.put("/**", "authc");
		sfBean.setFilterChainDefinitionMap(map);
		
		return sfBean;
		
	}
}

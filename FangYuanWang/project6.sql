/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.27 : Database - project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`project` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `project`;

/*Table structure for table `booklist` */

DROP TABLE IF EXISTS `booklist`;

CREATE TABLE `booklist` (
  `id` double NOT NULL AUTO_INCREMENT,
  `userName` varchar(765) DEFAULT NULL,
  `tel` varchar(33) DEFAULT NULL,
  `bookTime` datetime DEFAULT NULL,
  `manager` varchar(765) DEFAULT NULL,
  `managerTel` varchar(33) DEFAULT NULL,
  `address` varchar(765) DEFAULT NULL,
  `state` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `booklist` */

/*Table structure for table `dreamlist` */

DROP TABLE IF EXISTS `dreamlist`;

CREATE TABLE `dreamlist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '心愿单Id',
  `userId` bigint(20) NOT NULL COMMENT '用户Id',
  `houseId` bigint(20) DEFAULT NULL COMMENT '房子id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `dreamlist` */

insert  into `dreamlist`(`id`,`userId`,`houseId`) values (1,1,2);

/*Table structure for table `house` */

DROP TABLE IF EXISTS `house`;

CREATE TABLE `house` (
  `houseId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT ' 房子id',
  `ownId` bigint(20) DEFAULT NULL COMMENT '业主id',
  `region` varchar(255) DEFAULT NULL COMMENT '区域',
  `houseStyle` varchar(20) DEFAULT NULL COMMENT '房子样式',
  `houseMode` varchar(20) DEFAULT NULL COMMENT '房子出租方式',
  `bookTime` date DEFAULT NULL COMMENT '可预约时间',
  `createdTime` date DEFAULT NULL COMMENT '发布时间',
  `bed` varchar(50) DEFAULT NULL COMMENT '床',
  `heater` varchar(50) DEFAULT NULL COMMENT '热水器',
  `airConditioner` varchar(50) DEFAULT NULL COMMENT '空调',
  `refrigerator` varchar(50) DEFAULT NULL COMMENT '冰箱',
  `WiFi` varchar(50) DEFAULT NULL COMMENT 'WiFi',
  `metro` varchar(50) DEFAULT NULL COMMENT '地铁',
  `elevator` varchar(50) DEFAULT NULL COMMENT '电梯',
  `orientation` varchar(10) DEFAULT NULL COMMENT '朝向',
  `area` varchar(20) DEFAULT NULL COMMENT '面积',
  `picture1` varchar(50) DEFAULT NULL COMMENT '图1URL',
  `picture2` varchar(50) DEFAULT NULL COMMENT '图2URL',
  `picture3` varchar(50) DEFAULT NULL,
  `picture4` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`houseId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `house` */

insert  into `house`(`houseId`,`ownId`,`region`,`houseStyle`,`houseMode`,`bookTime`,`createdTime`,`bed`,`heater`,`airConditioner`,`refrigerator`,`WiFi`,`metro`,`elevator`,`orientation`,`area`,`picture1`,`picture2`,`picture3`,`picture4`) values (1,1,'广州番禺','两房一厅','整租','2020-09-14','2020-09-09','有','有','有','没有','有','靠近','有','朝南','30平方','/mig/picture01',NULL,NULL,NULL);

/*Table structure for table `owenid` */

DROP TABLE IF EXISTS `owenid`;

CREATE TABLE `owenid` (
  `ownerId` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '业主id',
  `ownerName` varchar(255) DEFAULT NULL COMMENT '用户名字',
  `salt` varchar(255) DEFAULT NULL COMMENT '盐值',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `tel` varchar(11) NOT NULL COMMENT '电话',
  `describe` varchar(255) DEFAULT NULL COMMENT '对租客要求',
  `verification` varchar(20) DEFAULT NULL COMMENT '信息验证',
  PRIMARY KEY (`ownerId`) USING BTREE,
  UNIQUE KEY `own_name` (`ownerName`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `owenid` */

insert  into `owenid`(`ownerId`,`ownerName`,`salt`,`password`,`tel`,`describe`,`verification`) values (1,'老王',NULL,'123456','1545252362','希望租客喜欢干净','房屋信息已验证'),(2,'老张',NULL,'456789','1548526320','对安静要求比较高','房屋信息未验证');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `userId` bigint(255) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) DEFAULT NULL COMMENT '用户名',
  `salt` varchar(255) DEFAULT NULL COMMENT '盐值',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `tel` varchar(11) NOT NULL COMMENT '联系电话',
  `describe` varchar(255) DEFAULT NULL,
  `statu` int(11) DEFAULT NULL,
  PRIMARY KEY (`userId`) USING BTREE,
  UNIQUE KEY `tel` (`tel`) USING BTREE,
  UNIQUE KEY `user_name` (`userName`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `user` */

insert  into `user`(`userId`,`userName`,`salt`,`password`,`tel`,`describe`,`statu`) values (1,'小张',NULL,'123456789','13656352303',NULL,NULL),(2,'',NULL,'','',NULL,NULL),(8,'小李李',NULL,'123456789','15425326032',NULL,NULL),(29,'小小熊小',NULL,'123456789','12352623562',NULL,NULL),(30,'小消息消息',NULL,'2d4c1e20a3b808f3400865e5f5a559b4','15452326230',NULL,NULL),(31,'消息小小小',NULL,'95f878d4606c3ae2e88c30cc51024ba4','15425202306',NULL,NULL),(32,'小小小小小','6c2b0f69-4763-461e-984c-540f859aa3f3','b8339b10a85ce27f6bc224970bae2e99','15452532620',NULL,NULL),(34,'MING','2222','22222','2222','',NULL),(35,'dd','dddd','ddddd','ddd','ddd',NULL),(37,'小王','7e0aef29-1f38-4f1e-a7ca-95e8a69377a1','5d53ca00c20e0fa204520ae6a10680aa','18783820632',NULL,NULL);

/*Table structure for table `user_owen` */

DROP TABLE IF EXISTS `user_owen`;

CREATE TABLE `user_owen` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `owen_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

/*Data for the table `user_owen` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

package com.tiger.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.HashSet;
import java.util.Set;

@Configuration
@PropertySource("classpath:/properties/redis.properties")
public class JedisConfig {
    @Value("${redis.nodes}")
    private String nodes;
    @Value("${redis.sentinel}")
    private String sentinel;
    @Value("${redis.clusters}")
    private String clusters;
    /*@Bean
    public ShardedJedis shardedJedis(){
        nodes=nodes.trim();//去除多余空格
        List<JedisShardInfo> list=new ArrayList<>();
        String[] nodeArray= nodes.split(",");
        for (String strNode:nodeArray) {
            String host=strNode.split(":")[0];
            Integer port=Integer.parseInt(strNode.split(":")[1]);
            JedisShardInfo info=new JedisShardInfo(host,port);
            list.add(info);
        }
        return new ShardedJedis(list);
    }*/
   /* @Value("${redis.host}")
    private String host;
    @Value("${redis.port}")
    private Integer port;
    @Bean
    public Jedis jedis(){

        return new Jedis(host,port);
    }*/

    /**
     * 配置哨兵
     * @return
     */
/*   @Bean
   public JedisSentinelPool jedisSentinelPool(){
       sentinel=sentinel.trim();//去除多余空格
       Set<String> set=new HashSet<>();
       set.add(sentinel);
       JedisSentinelPool sentinelPool=new JedisSentinelPool("mymaster",set);
      ;
       return sentinelPool;
   }
   */
   @Bean
    public JedisCluster jedisCluster(){
       Set<HostAndPort> nodes=new HashSet<>();
       String[] nodeArray= clusters.split(",");
       for (String strNode:nodeArray) {
           String host=strNode.split(":")[0];
           Integer port=Integer.parseInt(strNode.split(":")[1]);
           HostAndPort info=new HostAndPort(host,port);
           nodes.add(info);
       }
       return new JedisCluster(nodes);
   }
}

package com.tiger.service;


import com.tiger.pojo.JsonResult;
import com.tiger.pojo.book.Book;

import java.util.List;

public interface BookService {

	JsonResult doSave(Book book);

	List<Book> FindBook(String userName);

	int doDeleteObjectsByIds(Integer[] ids);

	int doUpdateObject(Book book);

}

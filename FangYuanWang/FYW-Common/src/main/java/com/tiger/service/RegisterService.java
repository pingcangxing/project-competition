package com.tiger.service;


import com.tiger.pojo.user.User;

public interface RegisterService {
	int saveRegister(User user);
	
	//获取四位验证码
	String verificationCode(String tel);
	
	String findByTel(String tel); 
}

package com.tiger.utils;

import java.util.Date;

public class TimeCheck {
	public static void hourCheck(Date date) {
	
	int hours = date.getHours();
	
	if(!(hours>=9&&hours<=17)) {
		throw new IllegalArgumentException("预约时间只能为：09:00-17:00");
	}
}
}
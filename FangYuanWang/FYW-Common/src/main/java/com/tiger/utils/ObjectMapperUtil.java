package com.tiger.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

public class ObjectMapperUtil {
    /**
     * 1.将用户传递的数据转为json串
     * 2.将redis传递的数据转为java对象
     */
    private static final ObjectMapper MAPPER=new ObjectMapper();
    public static String toJson(Object object){
        if(object==null){
            throw new RuntimeException("传递的数据为空，请检查");
        }
        try {
            String json = MAPPER.writeValueAsString(object);
            return json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }

    }

    public static <T> T toObj(String json,Class<T> target){
        if(StringUtils.isEmpty(json)||target==null){
            throw new RuntimeException("参数不能为空");
        }
        try {
            return MAPPER.readValue(json, target);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

}

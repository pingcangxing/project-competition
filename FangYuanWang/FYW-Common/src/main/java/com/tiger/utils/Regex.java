package com.tiger.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 校验正则表达式的utils类
 * @author 86187
 *
 */
public class Regex {
	/**
	 * 校验是否为正确的电话号码
	 * @param tel
	 * @return 正确不抛异常，错误抛异常
	 */
	public static void telRegex(String tel) {
		 

        String regex="^[1](([3][0-9])|([4][5,7,9])|([5][0-9])|([6][6])|([7][3,5,6,7,8])|([8][0-9])|([9][8,9]))[0-9]{8}$";// 验证手机号
		          if(tel.length() != 11){
		             throw new IllegalArgumentException("手机号应为11位数");
		          }else{
		              Pattern p = Pattern.compile(regex);
		             Matcher m = p.matcher(tel);
		             boolean isMatch = m.matches();
		             if(!isMatch){
		            	 throw new IllegalArgumentException("您的手机号:" + tel + "是错误格式！！！");
		             } 
		                 
		            
		         }
	}
}

package com.tiger.pojo.user;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@NoArgsConstructor
@Data
@Accessors(chain = true)
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3541900156315278677L;
	/**
	 * 
	 */
	
	private Integer userId;
	private String userName;
	private String salt;//盐值
	private String password;
	private String tel;
	private String describe;
	private Integer statu;//1,正常，2，删除

	public User(Integer userId, String userName, String salt, String password, String tel, String describe) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.salt = salt;
		this.password = password;
		this.tel = tel;
		this.describe = describe;
	}
	
	
	
	
}

package com.tiger.pojo;

import java.io.Serializable;



import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class JsonResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8815954602510535298L;
	private Integer state=1;
	private String message="success";
	private Object data;
	
	public JsonResult(Object data) {
	
		this.data = data;
	}
	public JsonResult(Throwable e) {
		this.state=0;
		this.message=e.getMessage();//获取异常信息
	}
	public JsonResult(String message) {
		super();
		this.message = message;
	}
}

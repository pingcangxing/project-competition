package com.tiger.pojo.book;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class Book implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4470312997642370740L;
	private Long id;
	private String userName;
	private String tel;
	@JsonFormat(pattern = "yyyy/MM/dd HH:mm",timezone = "GMT+8")
	private Date bookTime;
	private String manager;
	private String managerTel;
	private String address;
	private Integer state=1;
	
}

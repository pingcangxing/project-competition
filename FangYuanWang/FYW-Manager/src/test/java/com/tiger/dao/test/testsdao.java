package com.tiger.dao.test;

import com.tiger.dao.ManagerUserDao;
import com.tiger.pojo.ManagerUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class testsdao {

    @Autowired
    private ManagerUserDao managerUserDao;

    @Test
    public void testGetRowCount() {
        int rows=managerUserDao.getRowCount("李浩波");
        System.out.println("rows="+rows);

    }
    @Test
    public void test01(){
        List<ManagerUser> list=
                managerUserDao.findPageObjects("李浩波", 0, 3);
        for(ManagerUser log:list) {
            System.out.println(log);
        }
    }
}

package com.tiger.serviceuimpl;

import com.tiger.dao.ManagerUserDao;
import com.tiger.exception.ServiceException;
import com.tiger.pojo.ManagerUser;
import com.tiger.service.ManagerUserService;
import com.tiger.vo.PageObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sound.midi.Soundbank;
import java.util.List;

@Service
public class ManagerUserServiceImpl implements ManagerUserService{
    @Autowired
    private ManagerUserDao managerUserDao;

    @Override
    public int getRowCount(String username) {
        return 0;
    }

    @Override
    public PageObject<ManagerUser> findObjects(String username, Integer pageCurrent) {
        if (pageCurrent==null||pageCurrent<1)throw new IllegalArgumentException();
        int rowCount=managerUserDao.getRowCount(username);
        //System.out.println("asaassadsadasdasd"+rowCount);
        if(rowCount==0)
            throw new ServiceException("系统没有查到对应记录");
        int pageSize=10;
        int startIndex=(pageCurrent-1)*pageSize;
        List<ManagerUser> records=managerUserDao.findPageObjects(username,startIndex, pageSize);
        PageObject<ManagerUser> pageObject=new PageObject<>();
        pageObject.setPageCurrent(pageCurrent);
        pageObject.setPageSize(pageSize);
        pageObject.setRowCount(rowCount);
        pageObject.setRecords(records);
        pageObject.setPageCount((rowCount-1)/pageSize+1);
        return pageObject;
    }

    @Transactional(readOnly = true)

    public PageObject<ManagerUser> findPageObjects(String username, Integer pageCurrent) {
        String tName=Thread.currentThread().getName();
        System.out.println("SysUserService.findPageObjects.thread.name="+tName);
        //1.参数有效性校验
        if(pageCurrent==null||pageCurrent<1)
            throw new IllegalArgumentException("页码值无效");
        //2.查询总记录数并校验
        int rowCount=managerUserDao.getRowCount(username);
        if(rowCount==0)
            throw new ServiceException("没有找到对应记录");
        //3.查询当前页记录
        int pageSize=2;
        int startIndex=(pageCurrent-1)*pageSize;
        List<ManagerUser> records=
                managerUserDao.findPageObjects(username, startIndex, pageSize);
        //4.封装查询结果并返回
        return new PageObject<ManagerUser>(pageCurrent,pageSize,rowCount, records);
    }

}

package com.tiger.exception;

import java.io.Serializable;

public class ServiceException extends RuntimeException{
    public ServiceException() {
        super();
    }
    public ServiceException(String message) {
        super(message);
    }
    public ServiceException(Throwable cause) {
        super(cause);
    }
}

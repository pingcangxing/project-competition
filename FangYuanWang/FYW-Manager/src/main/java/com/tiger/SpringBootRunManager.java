package com.tiger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class SpringBootRunManager {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootRunManager.class, args);
    }
}

package com.tiger.service;

import com.tiger.pojo.ManagerUser;
import com.tiger.vo.PageObject;
import org.apache.ibatis.annotations.Param;

public interface ManagerUserService {

    int getRowCount(@Param("username") String username);

    PageObject<ManagerUser> findObjects(String username,Integer pageCurrent);
}

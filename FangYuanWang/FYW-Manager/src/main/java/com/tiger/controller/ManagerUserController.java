package com.tiger.controller;

import com.tiger.pojo.JsonResult;
import com.tiger.service.ManagerUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/log/")
public class ManagerUserController {
    @Autowired
    private ManagerUserService managerUserService;

    @RequestMapping("doFindPageObjects")
    @ResponseBody
    public JsonResult doFindPageObjects(String username,Integer pageCurrent){
        return new JsonResult(managerUserService.findObjects(username,pageCurrent));
    }
}

package com.tiger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/")
public class PageController {
    @RequestMapping("doLoginUI")
    public String doLoginUI() {
        return "login";
    }

    @RequestMapping("doManagerUI")
    public String doManagerUI() {
        return "starter";
    }
    @RequestMapping("{module}/{moduleUI}")
    public String doModuleUI(@PathVariable String moduleUI) {
        return "sys/"+moduleUI;
    }
    @RequestMapping("doPageUI")
    public String doPageUI() {
        return "common/page";
    }
}
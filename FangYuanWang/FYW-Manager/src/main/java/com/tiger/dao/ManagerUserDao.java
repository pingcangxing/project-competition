package com.tiger.dao;

import com.tiger.pojo.ManagerUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
@Mapper
public interface ManagerUserDao {
    @Select("select count(*) from manager_user")
    int getRowCount(@Param("username") String username);

    @Select("select *  from manager_user order by savetime desc limit #{startIndex},#{pageSize}")
    List<ManagerUser> findPageObjects(@Param("username") String username,
                                      @Param("startIndex")Integer starIndex,
                                      @Param("pageSize")Integer pageSize);

}

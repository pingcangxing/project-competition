package com.tiger.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManagerUser implements Serializable {

    private Integer Id;
    private String username;
    private String phone;
    private String demand;
    private Date savetime;
    private Integer price;





}

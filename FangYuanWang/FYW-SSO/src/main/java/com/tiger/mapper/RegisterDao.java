package com.tiger.mapper;

import com.tiger.pojo.user.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;



@Mapper
public interface RegisterDao {
	//保存注册数据
	@Insert("insert into user(userName,salt,password,tel) values(#{userName},#{salt},#{password},#{tel})")
	int saveRegister(User user);
	
	//根据电话号码查找信息
	@Select("select tel from user where tel=#{tel}")
	String findByTel(String tel); 
}

package com.tiger.mapper;


import com.tiger.pojo.user.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;



@Mapper
public interface LoginDao {

	@Select("select * from user where userName=#{userName}")
	User findObjects(String userName);
}

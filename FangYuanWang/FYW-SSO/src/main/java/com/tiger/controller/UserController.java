package com.tiger.controller;


import com.tiger.mapper.LoginDao;
import com.tiger.mapper.RegisterDao;

import com.tiger.pojo.user.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
public class UserController {
    @Autowired
    LoginDao loginDao;
    @Autowired
    RegisterDao registerDao;
    @GetMapping("/findUser/username")
    public User findUserByUserName(@RequestParam String username){
        User user = loginDao.findObjects(username);
        return user;
    }

    /**
     * 注册
     */
    @PostMapping("/saveRegister")
   public Integer saveRegister(@RequestBody User user){
        System.out.println(user);
        int row = registerDao.saveRegister(user);
        return row;
    }

    @GetMapping("/findByTel/tel")
    String findByTel(@RequestParam String tel){
        String newtel = registerDao.findByTel(tel);
        return newtel;
    }

}
